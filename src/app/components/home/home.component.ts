import {Component, OnInit} from '@angular/core';
import {BikeService} from '../../services/bike.service';
import {FormGroup, FormControl, Validators} from '@angular/forms';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  models: string[] = [
    'Globo MTB',
    'Globo carbon',
    'Globo time',
  ];
  bikeForm: FormGroup;
  validMessege = '';

  constructor(private bikeService: BikeService) {
  }

  ngOnInit() {
    this.bikeForm = new FormGroup({
      name: new FormControl('', Validators.required),
      email: new FormControl('', Validators.required),
      phone: new FormControl('', Validators.required),
      model: new FormControl('', Validators.required),
      serialNumber: new FormControl('', Validators.required),
      purchasePrice: new FormControl('', Validators.required),
      purchaseDate: new FormControl('', Validators.required),
      contact: new FormControl()
    });
  }

  submitRegistration() {
    if (this.bikeForm.valid) {
  this.validMessege = 'Your bike registration has been submitted. thanks';
  this.bikeService.createBikeRegistration(this.bikeForm.value).subscribe(
    data => {
      this.bikeForm.reset();
      return true;
    }, err => {
      return Observable.throw(err);
    }
  );
    } else {
      this.validMessege = 'Please fill out this form';
    }
  }

}
